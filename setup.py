from setuptools import setup, find_packages

setup(name='ellipsys2vtk',
      version='0.01',
      description='Reading EllipSys files into Python and VTK',
      author='Kenneth Loenbaek',
      author_email='kenloen@dtu.dk',
      license='',
      packages=find_packages(),
      zip_safe=False,
      install_requires = [
            'numpy',
            'pyvista'
      ]
      )
