# EllipSys2VTK
Python module for reading EllipSys files into python/VTK/PyVista.
## Installation
Before installing *EllipSys2VTK* it is recommended to install *PyVista* via conda install
as installing *PyVista/VTK* via pip can be difficult. Installing *PyVista*:
```
conda install -c conda-forge pyvista
```
After *PyVista* is installed the module can be installed via `pip`:
```
pip install git+https://gitlab.windenergy.dtu.dk/kenloen/ellipsys2vtk.git
```
## Basic usages
The module can load data in multiple data formats, with the easiest format to use being *PyVista*.
### PyVista
The main methods `XXD2pyvista` and `sim2pyvista`:
```python
>>> import ellipsys2vtk as e2v
>>> # Reading X3D file
>>> X3D = e2v.XXD2pyvista("grid.X3D")
>>> # Reading RST and X3D files (assuming same project name for both files)
>>> sim = e2v.sim2pyvista("grid.RST.01")
```
Both methods return `pyvista.MultiBlock` objects which is discribed further here: https://docs.pyvista.org/core/composite.html#multiblock-datasets
The filters available in *PyVista* can be seen here: https://docs.pyvista.org/core/filters.html
The `XXD2pyvista` method will by default load all data (points/x-y-z and attr/boundary-conditions) in the `.X3D` but is is not loading the
the ghost cells. This can be further controlled by the arguments for `XXD2pyvista`.
The `sim2pyvista` method will by default only load pressure (`p`) and velocities (`Vel`)
as well as the grid. It will not load the ghost cells by default. 
All the possible fields that can be loaded can be displayed by:
```python
>>> print(e2v.sim2names("grid.RST.01"))
```
As an example loading viscosity (`vis`) and density (`den`) is done as:
```python
>>> sim = e2v.sim2pyvista("grid.RST.01", {"vis", "den"})
```
Both `XXD2pyvista` and `sim2pyvista` can also only load a subset of the blocks by 
simply passing a list of the blocks that needs to be loaded 
(it starts from block 1 and not zero):
```python
>>> sim = e2v.sim2pyvista("grid.RST.01", blocks=[1, 3, 5])
```
The module also supports working with `.RST.` files obtained from different grid levels e.g. `grid.RST.02`. It automatically detects the grid level based on the file ending. 
```python
>>> sim = e2v.sim2pyvista("grid.RST.02")
```
To save a `pyvista.Multiblock` in `vtk` format:
```python
>>> sim.save('grid01.vtm')
```

### VTK
Similar to the PyVista methods, `XXD2vtk` and `sim2vtk` can be used to return a `vtkMultiBlockDataSet` object. The methods accept similar arguments as the PyVista methods. 
To save the data (e.g. for *ParaView*) the following routine can be followed
```python
>>> from vtkmodules.vtkIOXML import vtkXMLMultiBlockDataWriter
>>> # Reading RST and X3D files
>>> sim = e2v.sim2vtk("grid.RST.01")
>>> # Set up writer object and write data
>>> writer = vtkXMLMultiBlockDataWriter()
>>> writer.SetDataModeToAscii()
>>> writer.SetInputData(sim)
>>> writer.SetFileName('grid01.vtm')
>>> writer.Update()
```
### Python-Dictionaries
The module also has methods for loading the data as a python dictionary (`dict`) 
which are simply named as `*2dict` (e.g. `XXD2dict`, `sim2dict`).