import pyvista as pv
from .ellipsys2vtk import sim2vtk, XXD2vtk, adinput2vtk

def XXD2pyvista(filname, load_attr=True, blocks=None, load_ghost=False, is2D=None):
    """Reading EllipSys .X3D or .X2D files and returns a pyvista.MultiBlock.

    Parameters
    ----------
    filename : str  
        filename for a .X3D or.X2D file
    load_attr : bool, optional
        Flag for loading vertex attributes, by default True
    blocks : list, optional
        List of block to load. By default None -> load all blocks
    load_ghost : bool, optional
        Flag for loading ghost vertex points. `load_ghost=True`-> bsize+3, by default False -> bsize+1 
    is2D : bool, optional
        Flag for indicating if the data is 2D or not. By default None -> use file extension *.X2D* -> True

    Returns
    -------
    pyvista.MultiBlock
        PyVista Multi Block Data Set containing the vertex data and attributes if `load_attr=True`.
    """    
    return pv.MultiBlock(XXD2vtk(filname, load_attr, blocks, load_ghost, is2D))

def sim2pyvista(filename_RST, filename_XXD=None, load_names=None, read_default=True, blocks=None, load_ghost=False, is2D=None, vectors=None):
    """Reading EllipSys .RST.01 and .X3D (or .X2D) files and returns a pyvista.MultiBlock.

    Parameters
    ----------
    filename_RST : str  
        filename for a .RST file (currently only support .RST.01)
    filename_XXD : str  
        filename for a .X3D or.X2D file. By default None -> infere from filename_RST project-name. Ex. grid.RST.01 -> grid.X3D if in path otherwise grid.X2D
    load_names : set, optional
        Set of names to load. 
        If None it it will default to set("p", "Vel") if read_default=True, by default None
        options: (All options for a given RST file can be viewed with the RST2names)
            "attr": Load vertex attributes as *nblock x (bsize+1)**(3 or 2)* (reshaped as matrix if as_matrix=True)
            "p": Load pressure *nblock x bsize**(3 or 2)* (reshaped as matrix if as_matrix=True)
            "u": Load u-velocity component *nblock x bsize**(3 or 2)* (reshaped as matrix if as_matrix=True) - only need if only one component is needed
            "v": Load v-velocity component *nblock x bsize**(3 or 2)* (reshaped as matrix if as_matrix=True) - only need if only one component is needed
            "w": Load w-velocity component *nblock x bsize**(3 or 2)* (reshaped as matrix if as_matrix=True) - only need if only one component is needed
            "Vel": Load Velocity as a vector *nblock x 3 x bsize**(3 or 2)* (reshaped as matrix if as_matrix=True)
    read_default : bool, optional
        Flag for setting defaults, by default True
    blocks : list, optional
        List of block to load. By default None -> load all blocks
    load_ghost : bool, optional
        Flag for loading ghost vertex points. `load_ghost=True`-> bsize+3, by default False -> bsize+1 
    is2D : bool, optional
        Flag for indicating if the data is 2D or not. By default None -> use file extension *.X2D* -> True
    vectors : dict, optional
        Relationship between field names and vectors. Example: vector={"Vel":["u", "v", "w"]} will load a vector with the name Vel and the first component is u. By default None -> {"Vel":["u", "v", "w"]}

    Returns
    -------
    pyvista.MultiBlock
        PyVista Multi Block Data Set containing the vertex data along with the cell center data from the RST file.
    """    
    return pv.MultiBlock(sim2vtk(filename_RST, filename_XXD, load_names, read_default, blocks, load_ghost, is2D, vectors))

def adinput2pyvista(filename, ADs=None, close_disc=True):
    """Reads adinput.dat file and the associated output files if present

    Parameters
    ----------
    filename : str
        Filename (including path) of the adinput.dat file
    ADs : list, optional
        List of ADs to read. By default None -> read all ADs
    close_disc : bool, optional
        Flag for closing the AD disc. By default True

    Returns
    -------
    pyvista.MultiBlock
        PyVista Multi Block Data Set containing each of the ADs as well as the field data.
    """  
    return pv.MultiBlock(adinput2vtk(filename, ADs, close_disc))