import ellipsys2vtk
import unittest
import numpy as np
import os

test_path = os.path.join(os.path.dirname(__file__), "test_data")


class TestX3D2dict(unittest.TestCase):
    def test_default_read(self):
        X3D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "ADsim.X3D")
        )
        self.assertTrue("points" in X3D)  # key in dict
        self.assertFalse("attr" in X3D)
        self.assertTrue(len(X3D) == 1)
        np.testing.assert_equal(
            X3D["points"].shape, (nblock, 3, (bsize + 1) ** 3)
        )  # bsize +1 as it is vertex data

    def test_load_ghost(self):
        X3D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "ADsim.X3D"), load_ghost=True
        )
        self.assertTrue("points" in X3D)  # key in dict
        self.assertFalse("attr" in X3D)
        self.assertTrue(len(X3D) == 1)
        np.testing.assert_equal(
            X3D["points"].shape, (nblock, 3, (bsize + 3) ** 3)
        )  # bsize +1 as it is vertex data

    def test_read_attr(self):
        X3D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "ADsim.X3D"), {"attr"}
        )
        self.assertTrue("points" in X3D)  # key in dict
        self.assertTrue("attr" in X3D)
        self.assertTrue(len(X3D) == 2)
        np.testing.assert_equal(
            X3D["points"].shape, (nblock, 3, (bsize + 1) ** 3)
        )  # bsize +1 as it is vertex data
        np.testing.assert_equal(
            X3D["attr"].shape, (nblock, (bsize + 1) ** 3)
        )  # bsize +1 as it is vertex data

    def test_read_only_attr(self):
        X3D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "ADsim.X3D"), {"attr"}, False
        )
        self.assertFalse("points" in X3D)  # key in dict
        self.assertTrue("attr" in X3D)
        self.assertTrue(len(X3D) == 1)
        np.testing.assert_equal(
            X3D["attr"].shape, (nblock, (bsize + 1) ** 3)
        )  # bsize +1 as it is vertex data

    def test_read_xyz(self):
        X3D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "ADsim.X3D"), {"x", "y", "z"}, False
        )
        self.assertFalse("points" in X3D)  # key in dict
        self.assertFalse("attr" in X3D)
        self.assertTrue("x" in X3D)
        self.assertTrue("y" in X3D)
        self.assertTrue("z" in X3D)
        self.assertTrue(len(X3D) == 3)
        np.testing.assert_equal(
            X3D["x"].shape, (nblock, (bsize + 1) ** 3)
        )  # bsize +1 as it is vertex data
        np.testing.assert_equal(
            X3D["y"].shape, (nblock, (bsize + 1) ** 3)
        )  # bsize +1 as it is vertex data
        np.testing.assert_equal(
            X3D["z"].shape, (nblock, (bsize + 1) ** 3)
        )  # bsize +1 as it is vertex data

    def test_as_matrix_default(self):
        X3D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "ADsim.X3D"), as_matrix=True
        )
        self.assertTrue("points" in X3D)  # key in dict
        self.assertFalse("attr" in X3D)
        self.assertTrue(len(X3D) == 1)
        np.testing.assert_equal(
            X3D["points"].shape, (nblock, 3, bsize + 1, bsize + 1, bsize + 1)
        )

    def test_as_matrix_attr_xyz(self):
        X3D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "ADsim.X3D"),
            {"x", "y", "z", "attr"},
            False,
            as_matrix=True,
        )
        self.assertFalse("points" in X3D)  # key in dict
        self.assertTrue("attr" in X3D)
        self.assertTrue("x" in X3D)
        self.assertTrue("y" in X3D)
        self.assertTrue("z" in X3D)
        self.assertTrue(len(X3D) == 4)
        np.testing.assert_equal(
            X3D["attr"].shape, (nblock, bsize + 1, bsize + 1, bsize + 1)
        )
        np.testing.assert_equal(
            X3D["x"].shape, (nblock, bsize + 1, bsize + 1, bsize + 1)
        )
        np.testing.assert_equal(
            X3D["y"].shape, (nblock, bsize + 1, bsize + 1, bsize + 1)
        )
        np.testing.assert_equal(
            X3D["z"].shape, (nblock, bsize + 1, bsize + 1, bsize + 1)
        )

    def test_partial_blocks(self):
        blocks = [1, 2] + list(range(5, 8))
        nblock_in = len(blocks)
        X3D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "ADsim.X3D"), blocks=blocks
        )
        self.assertTrue("points" in X3D)
        self.assertEqual(nblock_in, X3D["points"].shape[0])
        np.testing.assert_equal(
            X3D["points"].shape, (nblock_in, 3, (bsize + 1) ** 3)
        )


class TestX2D2dict(unittest.TestCase):
    def test_default_read(self):
        X2D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "cyl.X2D")
        )
        self.assertTrue("points" in X2D)  # key in dict
        self.assertFalse("attr" in X2D)
        self.assertTrue(len(X2D) == 1)
        np.testing.assert_equal(
            X2D["points"].shape, (nblock, 3, (bsize + 1) ** 2)
        )  # bsize +1 as it is vertex data

    def test_load_ghost(self):
        X2D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "cyl.X2D"), load_ghost=True
        )
        self.assertTrue("points" in X2D)  # key in dict
        self.assertFalse("attr" in X2D)
        self.assertTrue(len(X2D) == 1)
        np.testing.assert_equal(
            X2D["points"].shape, (nblock, 3, (bsize + 3) ** 2)
        )  # bsize +1 as it is vertex data

    def test_read_attr(self):
        X2D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "cyl.X2D"), {"attr"}
        )
        self.assertTrue("points" in X2D)  # key in dict
        self.assertTrue("attr" in X2D)
        self.assertTrue(len(X2D) == 2)
        np.testing.assert_equal(
            X2D["points"].shape, (nblock, 3, (bsize + 1) ** 2)
        )  # bsize +1 as it is vertex data
        np.testing.assert_equal(
            X2D["attr"].shape, (nblock, (bsize + 1) ** 2)
        )  # bsize +1 as it is vertex data

    def test_read_only_attr(self):
        X2D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "cyl.X2D"), {"attr"}, False
        )
        self.assertFalse("points" in X2D)  # key in dict
        self.assertTrue("attr" in X2D)
        self.assertTrue(len(X2D) == 1)
        np.testing.assert_equal(
            X2D["attr"].shape, (nblock, (bsize + 1) ** 2)
        )  # bsize +1 as it is vertex data

    def test_read_xyz(self):
        X2D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "cyl.X2D"), {"x", "y", "z"}, False
        )
        self.assertFalse("points" in X2D)  # key in dict
        self.assertFalse("attr" in X2D)
        self.assertTrue("x" in X2D)
        self.assertTrue("y" in X2D)
        self.assertTrue("z" in X2D)
        self.assertTrue(len(X2D) == 3)
        np.testing.assert_equal(
            X2D["x"].shape, (nblock, (bsize + 1) ** 2)
        )  # bsize +1 as it is vertex data
        np.testing.assert_equal(
            X2D["y"].shape, (nblock, (bsize + 1) ** 2)
        )  # bsize +1 as it is vertex data
        np.testing.assert_equal(
            X2D["z"].shape, (nblock, (bsize + 1) ** 2)
        )  # bsize +1 as it is vertex data

    def test_as_matrix_default(self):
        X2D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "cyl.X2D"), as_matrix=True
        )
        self.assertTrue("points" in X2D)  # key in dict
        self.assertFalse("attr" in X2D)
        self.assertTrue(len(X2D) == 1)
        np.testing.assert_equal(
            X2D["points"].shape, (nblock, 3, bsize + 1, bsize + 1)
        )

    def test_as_matrix_attr_xyz(self):
        X2D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "cyl.X2D"),
            {"x", "y", "z", "attr"},
            False,
            as_matrix=True,
        )
        self.assertFalse("points" in X2D)  # key in dict
        self.assertTrue("attr" in X2D)
        self.assertTrue("x" in X2D)
        self.assertTrue("y" in X2D)
        self.assertTrue("z" in X2D)
        self.assertTrue(len(X2D) == 4)
        np.testing.assert_equal(
            X2D["attr"].shape, (nblock, bsize + 1, bsize + 1)
        )
        np.testing.assert_equal(X2D["x"].shape, (nblock, bsize + 1, bsize + 1))
        np.testing.assert_equal(X2D["y"].shape, (nblock, bsize + 1, bsize + 1))
        np.testing.assert_equal(X2D["z"].shape, (nblock, bsize + 1, bsize + 1))

    def test_partial_blocks(self):
        blocks = [1, 2] + list(range(5, 8))
        nblock_in = len(blocks)
        X2D, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "cyl.X2D"), blocks=blocks
        )
        self.assertTrue("points" in X2D)
        self.assertEqual(nblock_in, X2D["points"].shape[0])
        np.testing.assert_equal(
            X2D["points"].shape, (nblock_in, 3, (bsize + 1) ** 2)
        )


class TestRST2names(unittest.TestCase):
    def test_all_options(self):
        # all info
        all_info_data = ellipsys2vtk.ellipsys2dict.RST2names(
            os.path.join(test_path, "ADsim.RST.01"), "all info"
        )
        self.assertTrue(isinstance(all_info_data, dict))
        self.assertTrue("u" in all_info_data)
        self.assertTrue("v" in all_info_data)
        self.assertTrue("w" in all_info_data)
        self.assertTrue("p" in all_info_data)
        self.assertTrue("den" in all_info_data)
        self.assertTrue("vis" in all_info_data)
        self.assertTrue("nblock" in all_info_data)
        self.assertTrue("bsize" in all_info_data)
        # all
        all_data = ellipsys2vtk.ellipsys2dict.RST2names(
            os.path.join(test_path, "ADsim.RST.01"), "all"
        )
        self.assertTrue(isinstance(all_data, set))
        self.assertEqual(len(all_info_data), len(all_data))
        for name in all_data:
            self.assertTrue(name in all_info_data)

        # fields info
        fields_info = ellipsys2vtk.ellipsys2dict.RST2names(
            os.path.join(test_path, "ADsim.RST.01"), "fields info"
        )
        self.assertTrue(isinstance(fields_info, dict))
        self.assertTrue(len(fields_info) < len(all_info_data))
        for name, info in fields_info.items():
            for key, val in info.items():
                self.assertEqual(all_info_data[name][key], val)
        self.assertTrue("u" in fields_info)
        self.assertTrue("v" in fields_info)
        self.assertTrue("w" in fields_info)
        self.assertTrue("p" in fields_info)
        self.assertTrue("den" in fields_info)
        self.assertTrue("vis" in fields_info)
        self.assertFalse("nblock" in fields_info)
        self.assertFalse("bsize" in fields_info)
        # fields (default)
        fields = ellipsys2vtk.ellipsys2dict.RST2names(
            os.path.join(test_path, "ADsim.RST.01")
        )
        self.assertTrue(isinstance(fields, set))
        self.assertTrue(len(fields) < len(all_data))
        self.assertTrue(len(fields), len(fields_info))
        for name in fields:
            self.assertTrue(name in fields_info)


class Testsim2names(unittest.TestCase):
    def test_all_options(self):
        # all info
        all_info_data_RST = ellipsys2vtk.ellipsys2dict.RST2names(
            os.path.join(test_path, "ADsim.RST.01"), "all info"
        )
        all_info_data = ellipsys2vtk.ellipsys2dict.sim2names(
            os.path.join(test_path, "ADsim.RST.01"), "all info"
        )
        self.assertEqual(len(all_info_data_RST), len(all_info_data) - 1)
        self.assertTrue("attr" in all_info_data)
        self.assertFalse("attr" in all_info_data_RST)
        for name, val in all_info_data_RST.items():
            np.testing.assert_equal(val, all_info_data[name])
        # all
        all_data_RST = ellipsys2vtk.ellipsys2dict.RST2names(
            os.path.join(test_path, "ADsim.RST.01"), "all"
        )
        all_data = ellipsys2vtk.ellipsys2dict.sim2names(
            os.path.join(test_path, "ADsim.RST.01"), "all"
        )
        self.assertEqual(len(all_data_RST), len(all_data) - 1)
        self.assertTrue("attr" in all_data)
        self.assertFalse("attr" in all_data_RST)
        for name in all_data_RST:
            self.assertTrue(name in all_data)

        # fields info
        field_info_data_RST = ellipsys2vtk.ellipsys2dict.RST2names(
            os.path.join(test_path, "ADsim.RST.01"), "fields info"
        )
        field_info_data = ellipsys2vtk.ellipsys2dict.sim2names(
            os.path.join(test_path, "ADsim.RST.01"), "fields info"
        )
        self.assertEqual(len(field_info_data_RST), len(field_info_data) - 1)
        self.assertTrue("attr" in field_info_data)
        self.assertFalse("attr" in field_info_data_RST)
        for name, val in field_info_data_RST.items():
            np.testing.assert_equal(val, field_info_data[name])
        # fields (default)
        fields_data_RST = ellipsys2vtk.ellipsys2dict.RST2names(
            os.path.join(test_path, "ADsim.RST.01")
        )
        fields_data = ellipsys2vtk.ellipsys2dict.sim2names(
            os.path.join(test_path, "ADsim.RST.01")
        )
        self.assertEqual(len(fields_data_RST), len(fields_data) - 1)
        self.assertTrue("attr" in fields_data)
        self.assertFalse("attr" in fields_data_RST)
        for name in fields_data_RST:
            self.assertTrue(name in fields_data)


class TestRST2dict(unittest.TestCase):
    def test_default(self):
        RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
            os.path.join(test_path, "ADsim.RST.01")
        )
        self.assertTrue("Vel" in RST)
        self.assertTrue("p" in RST)
        self.assertTrue(RST["p"].dtype == float)
        self.assertTrue(RST["Vel"].dtype == float)
        np.testing.assert_equal(
            RST["Vel"].shape, (nblock, 3, (bsize + 1) ** 3)
        )
        np.testing.assert_equal(RST["p"].shape, (nblock, (bsize + 1) ** 3))

    def test_load_ghost(self):
        RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
            os.path.join(test_path, "ADsim.RST.01"), load_ghost=True
        )
        self.assertTrue("Vel" in RST)
        self.assertTrue("p" in RST)
        self.assertTrue(RST["p"].dtype == float)
        self.assertTrue(RST["Vel"].dtype == float)
        np.testing.assert_equal(
            RST["Vel"].shape, (nblock, 3, (bsize + 3) ** 3)
        )
        np.testing.assert_equal(RST["p"].shape, (nblock, (bsize + 3) ** 3))

    def test_blocks(self):
        blocks = [1, 2] + list(range(5, 8))
        nblock_out = len(blocks)
        RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
            os.path.join(test_path, "ADsim.RST.01"), blocks=blocks
        )
        self.assertTrue("Vel" in RST)
        self.assertTrue("p" in RST)
        self.assertTrue(RST["p"].dtype == float)
        self.assertTrue(RST["Vel"].dtype == float)
        np.testing.assert_equal(
            RST["Vel"].shape, (nblock_out, 3, (bsize + 1) ** 3)
        )
        np.testing.assert_equal(RST["p"].shape, (nblock_out, (bsize + 1) ** 3))

    def test_read_non_default_fields(self):
        names = {"fa_PJix", "fa_PJiy", "fa_PJiz"}
        RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
            os.path.join(test_path, "ADsim.RST.01"), names, read_default=False
        )
        self.assertFalse("Vel" in RST)
        self.assertFalse("p" in RST)
        for name in names:
            self.assertTrue(name in RST)
            self.assertTrue(RST[name].dtype == float)
            np.testing.assert_equal(
                RST[name].shape, (nblock, (bsize + 1) ** 3)
            )

    def test_read_non_default_fields_as_vec(self):
        vector = dict(fa_PJi=["fa_PJix", "fa_PJiy", "fa_PJiz"])
        names = {"fa_PJi"}
        RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
            os.path.join(test_path, "ADsim.RST.01"),
            names,
            read_default=False,
            vectors=vector,
        )
        self.assertFalse("Vel" in RST)
        self.assertFalse("p" in RST)
        for name in names:
            self.assertTrue(name in RST)
            self.assertTrue(RST[name].dtype == float)
            np.testing.assert_equal(
                RST[name].shape, (nblock, 3, (bsize + 1) ** 3)
            )

    def test_as_matrix(self):
        RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
            os.path.join(test_path, "ADsim.RST.01"), as_matrix=True
        )
        self.assertTrue("Vel" in RST)
        self.assertTrue("p" in RST)
        self.assertTrue(RST["p"].dtype == float)
        self.assertTrue(RST["Vel"].dtype == float)
        np.testing.assert_equal(
            RST["Vel"].shape, (nblock, 3, bsize + 1, bsize + 1, bsize + 1)
        )
        np.testing.assert_equal(
            RST["p"].shape, (nblock, bsize + 1, bsize + 1, bsize + 1)
        )

    def test_default_2D(self):
        RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
            os.path.join(test_path, "cyl.RST.01"), is2D=True
        )
        self.assertTrue("Vel" in RST)
        self.assertTrue("p" in RST)
        self.assertTrue(RST["p"].dtype == float)
        self.assertTrue(RST["Vel"].dtype == float)
        np.testing.assert_equal(
            RST["Vel"].shape, (nblock, 3, (bsize + 1) ** 2)
        )
        np.testing.assert_equal(RST["p"].shape, (nblock, (bsize + 1) ** 2))

    def test_load_ghost_2D(self):
        RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
            os.path.join(test_path, "cyl.RST.01"), is2D=True, load_ghost=True
        )
        self.assertTrue("Vel" in RST)
        self.assertTrue("p" in RST)
        self.assertTrue(RST["p"].dtype == float)
        self.assertTrue(RST["Vel"].dtype == float)
        np.testing.assert_equal(
            RST["Vel"].shape, (nblock, 3, (bsize + 3) ** 2)
        )
        np.testing.assert_equal(RST["p"].shape, (nblock, (bsize + 3) ** 2))

    def test_blocks_2D(self):
        blocks = [1, 2] + list(range(5, 8))
        nblock_out = len(blocks)
        RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
            os.path.join(test_path, "cyl.RST.01"), blocks=blocks, is2D=True
        )
        self.assertTrue("Vel" in RST)
        self.assertTrue("p" in RST)
        self.assertTrue(RST["p"].dtype == float)
        self.assertTrue(RST["Vel"].dtype == float)
        np.testing.assert_equal(
            RST["Vel"].shape, (nblock_out, 3, (bsize + 1) ** 2)
        )
        np.testing.assert_equal(RST["p"].shape, (nblock_out, (bsize + 1) ** 2))

    def test_read_non_default_fields_2D(self):
        names = {"flowksi", "flowksi1"}
        RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
            os.path.join(test_path, "cyl.RST.01"),
            names,
            read_default=False,
            is2D=True,
        )
        self.assertFalse("Vel" in RST)
        self.assertFalse("p" in RST)
        for name in names:
            self.assertTrue(name in RST)
            self.assertTrue(RST[name].dtype == float)
            np.testing.assert_equal(
                RST[name].shape, (nblock, (bsize + 1) ** 2)
            )

    def test_read_non_default_fields_as_vec_2D(self):
        vector = dict(flowksi=["flowksi", "flowksi1"])
        names = {"flowksi"}
        RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
            os.path.join(test_path, "cyl.RST.01"),
            names,
            read_default=False,
            vectors=vector,
            is2D=True,
        )
        self.assertFalse("Vel" in RST)
        self.assertFalse("p" in RST)
        for name in names:
            self.assertTrue(name in RST)
            self.assertTrue(RST[name].dtype == float)
            np.testing.assert_equal(
                RST[name].shape, (nblock, 3, (bsize + 1) ** 2)
            )

    def test_as_matrix_2D(self):
        RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
            os.path.join(test_path, "cyl.RST.01"), as_matrix=True, is2D=True
        )
        self.assertTrue("Vel" in RST)
        self.assertTrue("p" in RST)
        self.assertTrue(RST["p"].dtype == float)
        self.assertTrue(RST["Vel"].dtype == float)
        np.testing.assert_equal(
            RST["Vel"].shape, (nblock, 3, bsize + 1, bsize + 1)
        )
        np.testing.assert_equal(RST["p"].shape, (nblock, bsize + 1, bsize + 1))

    def test_fail_read_dim(self):
        with self.assertRaises(ValueError):  # 2D without flag
            RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
                os.path.join(test_path, "cyl.RST.01")
            )

        with self.assertRaises(ValueError):  # 3D with 2D flag
            RST, bsize, nblock = ellipsys2vtk.ellipsys2dict.RST2dict(
                os.path.join(test_path, "ADsim.RST.01"), is2D=True
            )


class Testsim2dict(unittest.TestCase):
    def test_default(self):
        sim, bsize, nblock = ellipsys2vtk.ellipsys2dict.sim2dict(
            os.path.join(test_path, "ADsim.RST.01")
        )
        self.assertTrue("points" in sim)
        self.assertTrue("Vel" in sim)
        self.assertTrue("p" in sim)
        self.assertEqual(len(sim), 3)
        # Test 3D
        np.testing.assert_equal(
            sim["points"].shape, (nblock, 3, (bsize + 1) ** 3)
        )

    def test_default_2D(self):
        sim, bsize, nblock = ellipsys2vtk.ellipsys2dict.sim2dict(
            os.path.join(test_path, "cyl.RST.01")
        )
        self.assertTrue("points" in sim)
        self.assertTrue("Vel" in sim)
        self.assertTrue("p" in sim)
        self.assertEqual(len(sim), 3)
        # Test 2D
        np.testing.assert_equal(
            sim["points"].shape, (nblock, 3, (bsize + 1) ** 2)
        )

    def test_fail(self):
        # No grid file
        with self.assertRaises(ValueError):
            sim, bsize, nblock = ellipsys2vtk.ellipsys2dict.sim2dict(
                "non.RST.01"
            )
        # Not matching XXD and RST in size
        with self.assertRaises(ValueError):
            sim, bsize, nblock = ellipsys2vtk.ellipsys2dict.sim2dict(
                os.path.join(test_path, "ADsim.RST.01"),
                os.path.join(test_path, "cyl.X2D"),
            )

    def test_all_input(self):
        fname_RST = os.path.join(test_path, "cyl.RST.01")
        fname_XXD = os.path.join(test_path, "cyl.X2D")
        load_names = {"attr", "vis", "velocity"}
        read_default = False
        blocks = [1, 2] + list(range(5, 8))
        load_ghost = True
        as_matrix = True
        is2D = True
        vectors = dict(velocity=["u", "v", "w"])
        sim, bsize, nblock = ellipsys2vtk.ellipsys2dict.sim2dict(
            fname_RST,
            fname_XXD,
            load_names,
            read_default,
            blocks,
            load_ghost,
            as_matrix,
            is2D,
            vectors,
        )
        X2D = ellipsys2vtk.ellipsys2dict.XXD2dict(
            fname_XXD,
            load_names,
            read_default,
            blocks,
            load_ghost,
            as_matrix,
            is2D,
        )[0]
        RST = ellipsys2vtk.ellipsys2dict.RST2dict(
            fname_RST,
            load_names,
            read_default,
            blocks,
            load_ghost,
            as_matrix,
            is2D,
            vectors,
        )[0]
        self.assertEqual(len(sim), len(X2D) + len(RST))
        self.assertEqual(len(sim), 3)
        for name, val in X2D.items():
            np.testing.assert_equal(val, sim[name])
        for name, val in RST.items():
            np.testing.assert_equal(val, sim[name])

    def test_gridlevels(self):
        gridlevels = [2,3,4]
        fname_XXD = os.path.join(test_path, "AD_gl_test.X3D")
        load_names = {"attr", "vis", "velocity"}
        read_default = False
        blocks = [1, 2] + list(range(5, 8))
        load_ghost = True
        as_matrix = True
        is2D = False
        vectors = dict(velocity=["u", "v", "w"])
        load_ghost=True

        for gridlevel in gridlevels:
            fname_RST = os.path.join(test_path, 'AD_gl_test.RST.0'+str(gridlevel))
            sim, bsize, nblock = ellipsys2vtk.ellipsys2dict.sim2dict(
                fname_RST,
                fname_XXD,
                load_names,
                read_default,
                blocks,
                load_ghost,
                as_matrix,
                is2D,
                vectors)
            X3D, bsize_X3D, nblock_X3D = ellipsys2vtk.ellipsys2dict.XXD2dict(
                fname_XXD,
                load_names,
                read_default,
                blocks,
                load_ghost,
                as_matrix,
                is2D,)
            RST, bsize_RST, nblock_RST = ellipsys2vtk.ellipsys2dict.RST2dict(
                        fname_RST,
                        load_names,
                        read_default,
                        blocks,
                        load_ghost,
                        as_matrix,
                        is2D,
                        vectors,)

            self.assertEqual(bsize,bsize_RST)
            self.assertEqual(bsize*2**(gridlevel-1),bsize_X3D)

class Testadinput2dict(unittest.TestCase):
    def test_default(self):
        out = ellipsys2vtk.ellipsys2dict.adinput2dict(
            os.path.join(test_path, "AD_model/adinput.dat")
        )

        self.assertTrue("AD1" in out)
        self.assertTrue("AD2" in out)
        self.assertTrue("FN" in out["AD1"]["discgrid"])
        self.assertTrue("FT" in out["AD1"]["discgrid"])
        self.assertTrue("VT" in out["AD1"]["discgrid"])
        self.assertTrue("VN" in out["AD1"]["discgrid"])
        self.assertTrue("points" in out["AD1"]["discgrid"])

    def test_ADs(self):
        out = ellipsys2vtk.ellipsys2dict.adinput2dict(
            os.path.join(test_path, "AD_model/adinput.dat"), [2]
        )
        self.assertFalse("AD1" in out)
        self.assertTrue("AD2" in out)

    def test_closedisc(self):
        out_wcd = ellipsys2vtk.ellipsys2dict.adinput2dict(
            os.path.join(test_path, "AD_model/adinput.dat"),
            close_disc=True,
            as_matrix=True,
        )
        out = ellipsys2vtk.ellipsys2dict.adinput2dict(
            os.path.join(test_path, "AD_model/adinput.dat"),
            close_disc=False,
            as_matrix=True,
        )

        shape = out["AD1"]["discgrid"]["points"].shape
        shape_wcd = out_wcd["AD1"]["discgrid"]["points"].shape
        np.testing.assert_equal(shape[0], shape_wcd[0])
        np.testing.assert_equal(shape[1] + 1, shape_wcd[1])


if __name__ == "__main__":
    unittest.main()
