import os.path
import io

import ellipsys2vtk
import unittest
import numpy as np
from ellipsys2vtk.tests.test_ellipsys2dict import test_path


class Testsim2pyvista(unittest.TestCase):
    def test_default(self):
        simdict, bsize, nblock = ellipsys2vtk.ellipsys2dict.sim2dict(
            os.path.join(test_path, "ADsim.RST.01")
        )
        sim = ellipsys2vtk.sim2pyvista(os.path.join(test_path, "ADsim.RST.01"))
        for name in simdict.keys():  # Testing if names are in dataset
            if name == "points":
                continue
            sim[0][name]
        np.testing.assert_equal(sim[0]["Vel"].T.shape, simdict["Vel"][0].shape)
        # Testing names
        for iblock in range(1, nblock + 1):
            self.assertEqual(
                "Block %d" % iblock, sim.get_block_name(iblock - 1)
            )

    def test_all_inputs(self):
        fname_RST = os.path.join(test_path, "ADsim.RST.01")
        fname_XXD = os.path.join(test_path, "ADsim.X3D")
        load_names = {"attr", "vis", "velocity"}
        read_default = False
        blocks = [1, 2] + list(range(5, 8))
        load_ghost = True
        is2D = False
        vectors = dict(velocity=["u", "v", "w"])
        simdict, bsize, nblock = ellipsys2vtk.ellipsys2dict.sim2dict(
            fname_RST,
            fname_XXD,
            load_names,
            read_default,
            blocks,
            load_ghost,
            False,
            is2D,
            vectors,
        )
        sim = ellipsys2vtk.sim2pyvista(
            fname_RST,
            fname_XXD,
            load_names,
            read_default,
            blocks,
            load_ghost,
            is2D,
            vectors,
        )
        for name in simdict.keys():  # Testing if names are in dataset
            if name == "points":
                continue
            sim[0][name]
        np.testing.assert_equal(
            sim[0]["velocity"].T.shape, simdict["velocity"][0].shape
        )
        # Testing names
        for ib, iblock in enumerate(blocks):
            self.assertEqual("Block %d" % iblock, sim.get_block_name(ib))

    def test_default_2D(self):
        simdict, bsize, nblock = ellipsys2vtk.ellipsys2dict.sim2dict(
            os.path.join(test_path, "cyl.RST.01")
        )
        sim = ellipsys2vtk.sim2pyvista(os.path.join(test_path, "cyl.RST.01"))
        for name in simdict.keys():  # Testing if names are in dataset
            if name == "points":
                continue
            sim[0][name]
        np.testing.assert_equal(sim[0]["Vel"].T.shape, simdict["Vel"][0].shape)
        # Testing names
        for iblock in range(1, nblock + 1):
            self.assertEqual(
                "Block %d" % iblock, sim.get_block_name(iblock - 1)
            )

    def test_all_inputs_2D(self):
        fname_RST = os.path.join(test_path, "cyl.RST.01")
        fname_XXD = os.path.join(test_path, "cyl.X2D")
        load_names = {"attr", "vis", "velocity"}
        read_default = False
        blocks = [1, 2] + list(range(5, 8))
        load_ghost = True
        is2D = True
        vectors = dict(velocity=["u", "v", "w"])
        simdict, bsize, nblock = ellipsys2vtk.ellipsys2dict.sim2dict(
            fname_RST,
            fname_XXD,
            load_names,
            read_default,
            blocks,
            load_ghost,
            False,
            is2D,
            vectors,
        )
        sim = ellipsys2vtk.sim2pyvista(
            fname_RST,
            fname_XXD,
            load_names,
            read_default,
            blocks,
            load_ghost,
            is2D,
            vectors,
        )
        for name in simdict.keys():  # Testing if names are in dataset
            if name == "points":
                continue
            sim[0][name]
        np.testing.assert_equal(
            sim[0]["velocity"].T.shape, simdict["velocity"][0].shape
        )
        # Testing names
        for ib, iblock in enumerate(blocks):
            self.assertEqual("Block %d" % iblock, sim.get_block_name(ib))


class TestXXD2pyvista(unittest.TestCase):
    def test_default(self):
        XXDdict, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "ADsim.X3D"), {"attr"}
        )
        XXD = ellipsys2vtk.XXD2pyvista(os.path.join(test_path, "ADsim.X3D"))
        for name in XXDdict.keys():  # Testing if names are in dataset
            if name == "points":
                continue
            XXD[0][name]
        np.testing.assert_equal(XXD[0]["attr"].shape, XXDdict["attr"][0].shape)
        # Testing names
        for iblock in range(1, nblock + 1):
            self.assertEqual(
                "Block %d" % iblock, XXD.get_block_name(iblock - 1)
            )

    def test_all_inputs(self):
        filename = os.path.join(test_path, "ADsim.X3D")
        load_attr = False
        blocks = [1, 2] + list(range(5, 8))
        load_ghost = True
        is2D = False
        XXDdict, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            filename, blocks=blocks, load_ghost=load_ghost, is2D=is2D
        )
        XXD = ellipsys2vtk.XXD2pyvista(
            filename, load_attr, blocks, load_ghost, is2D
        )
        np.testing.assert_equal(XXD[0].points.T, XXDdict["points"][0])
        # Testing names
        for ib, iblock in enumerate(blocks):
            self.assertEqual("Block %d" % iblock, XXD.get_block_name(ib))

    def test_default_2D(self):
        XXDdict, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            os.path.join(test_path, "cyl.X2D"), {"attr"}
        )
        XXD = ellipsys2vtk.XXD2pyvista(os.path.join(test_path, "cyl.X2D"))
        for name in XXDdict.keys():  # Testing if names are in dataset
            if name == "points":
                continue
            XXD[0][name]
        np.testing.assert_equal(XXD[0]["attr"].shape, XXDdict["attr"][0].shape)
        # Testing names
        for iblock in range(1, nblock + 1):
            self.assertEqual(
                "Block %d" % iblock, XXD.get_block_name(iblock - 1)
            )

    def test_all_inputs_2D(self):
        filename = os.path.join(test_path, "cyl.X2D")
        load_attr = False
        blocks = [1, 2] + list(range(5, 8))
        load_ghost = True
        is2D = True
        XXDdict, bsize, nblock = ellipsys2vtk.ellipsys2dict.XXD2dict(
            filename, blocks=blocks, load_ghost=load_ghost, is2D=is2D
        )
        XXD = ellipsys2vtk.XXD2pyvista(
            filename, load_attr, blocks, load_ghost, is2D
        )
        np.testing.assert_equal(XXD[0].points.T, XXDdict["points"][0])
        # Testing names
        for ib, iblock in enumerate(blocks):
            self.assertEqual("Block %d" % iblock, XXD.get_block_name(ib))


def test_adinput_rot_trans(tmp_path):
    filename = os.path.join(tmp_path, "ad_dummy.dat")
    o1s = [
        [1.0, 0.0, 0.0],
        [0.0, 1.0, 0.0],
        [0.0, 0.0, 1.0],
        [0.0, 0.5, 0.5],
        [0.0, 0.25, 0.5],
        [1.0, 0.0, 0.0],
        [1.0, 0.0, 0.0],
    ]
    o2s = [
        [0.0, 0.0, 1.0],
        [0.0, 0.0, 1.0],
        [1.0, 0.0, 0.0],
        [1.0, 0.0, 0.0],
        [1.0, 0.0, 0.0],
        [0.0, 0.5, 0.5],
        [0.0, 0.25, 0.5],
    ]
    for o1, o2 in zip(o1s, o2s):
        write_adinput_test(filename, o1, o2)
        o3 = np.cross(o1, o2)
        ad = ellipsys2vtk.adinput2pyvista(filename, close_disc=False)[0]
        data = ad.points.reshape((100, 2, 3))
        # At 0,0,0
        np.testing.assert_almost_equal(data[0, 0, :], 0)
        # Match o2
        o2_cal = -data[0, 1, :] / np.linalg.norm(data[0, 1, :])
        np.testing.assert_almost_equal(o2_cal, o2 / np.linalg.norm(o2))
        # Match o1
        o3_cal = data[int(len(data[:, 0, 0]) / 4), 1, :]
        o1_cal = np.cross(o3_cal, o2_cal)
        o1_cal /= np.linalg.norm(o1_cal)
        np.testing.assert_almost_equal(o1_cal, o1 / np.linalg.norm(o1))


def test_translate_scale(tmp_path):
    o1 = [0.0, 0.0, 1.0]
    o2 = [1.0, 0.0, 0.0]
    xyz = [10.0, 20.0, 30.0]
    D = 50
    filename = os.path.join(tmp_path, "ad_dummy.dat")
    write_adinput_test(filename, o1, o2, xyz, D)
    ad = ellipsys2vtk.adinput2pyvista(filename, close_disc=False)[0]
    data = ad.points.reshape((100, 2, 3))
    # Translation
    np.testing.assert_almost_equal(data[0, 0, :], xyz)
    # Scaling
    D_cal = np.linalg.norm(data[0, 0, :] - data[0, -1, :]) * 2
    assert D_cal == D


def test_all_transform(tmp_path):
    o1 = [0.0, 0.5, 0.25]
    o2 = [1.0, 0.0, 0.0]
    xyz = [10.0, 20.0, 30.0]
    D = 50
    filename = os.path.join(tmp_path, "ad_dummy.dat")
    write_adinput_test(filename, o1, o2, xyz, D)
    ad = ellipsys2vtk.adinput2pyvista(filename, close_disc=False)[0]
    data = ad.points.reshape((100, 2, 3))
    # Translation
    np.testing.assert_almost_equal(data[0, 0, :], xyz)
    # Scaling
    D_cal = np.linalg.norm(data[0, 0, :] - data[0, -1, :]) * 2
    assert D_cal == D
    # o2
    o2_cal = -(data[0, 1, :] - data[0, 0, :])
    o2_cal /= np.linalg.norm(o2_cal)
    np.testing.assert_almost_equal(o2_cal, o2 / np.linalg.norm(o2))
    # o1
    io3 = int(len(data[:, 0, 0]) / 4)
    o3_cal = data[io3, 1, :] - data[0, 0, :]
    o1_cal = np.cross(o3_cal, o2_cal)
    o1_cal /= np.linalg.norm(o1_cal)
    np.testing.assert_almost_equal(o1_cal, o1 / np.linalg.norm(o1))


def write_adinput_test(filename, o1, o2, xyz=None, D=2.0):
    if xyz is None:
        xyz = [0, 0, 0]
    with open(filename, "w") as file:
        file.writelines(
            "# name          grid                                    D       "
            + "        x                y "
            "               z      o11       o12       o13      o21       o22"
            + "       o23 loading\n"
        )
        file.writelines(
            "no-name dummy_grid.dat %s %s %s %s %s %s %s %s %s %s"
            % (
                D,
                xyz[0],
                xyz[1],
                xyz[2],
                o1[0],
                o1[1],
                o1[2],
                o2[0],
                o2[1],
                o2[2],
            )
        )


if __name__ == "__main__":
    unittest.main()
