import numpy as np
from .util import FortranFile_wskip
import os


### -------------------------- X3D and X2D files -------------------------- ###
def XXD2dict(filename, load_names=None, read_default=True, blocks=None,
             load_ghost=False, as_matrix=False, is2D=None):
    """Reading EllipSys .X3D or .X2D files and return dict with data and bsize
    and nblock

    Parameters
    ----------
    filename : str
        filename for a .X3D or.X2D file
    load_names : set, optional
        Set of names to load.
        If None it it will default to set("points") if read_default=True,
        by default None
        options:
            "points": Load all points as *nblock x 3 x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
            "attr": Load vertex attributes as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
            "x": Load x-vertex coordinates as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - not loaded if points are in set
            "y": Load y-vertex coordinates as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - not loaded if points are in set
            "z": Load z-vertex coordinates as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - not loaded if points are in set
    read_default : bool, optional
        Flag for setting defaults, by default True
    blocks : list, optional
        List of block to load. By default None -> load all blocks
    load_ghost : bool, optional
        Flag for loading ghost vertex points. `load_ghost=True`-> bsize+3,
        by default False -> bsize+1
    as_matrix : bool, optional
        Flag for making the output as matrix
        *nblock x bsize+1 x bsize+1 (x bsize+1)* ,
        by default False
    is2D : bool, optional
        Flag for indicating if the data is 2D or not.
        By default None -> use file extension *.X2D* -> True

    Returns
    -------
    data_out: dict
        output data, see load_names for information
    bsize: int
        Block size (number of nodes -> +1 for number of vertices)
    nblock: int
        Number of blocks in the file (not the number of blocks loaded)
    """
    load_names, is2D = XXD_set_default_args(
        filename, load_names, read_default, is2D
    )

    # Reading X3D file
    data_out = dict()
    with FortranFile_wskip(filename, "r") as file:
        # Reading header
        bsize = int(file.read_ints()[0])
        nblock = int(file.read_ints()[0])
        if is2D:  # Not sure what this line is for in the 2D grid?! - Kenneth
            _ = file.read_ints()

        if blocks is None:
            blocks = range(1, nblock + 1)

        nblock_out = len(blocks)

        # Inialize arrays
        ppbs_wghost = bsize + 3  # +3 -> 2 for ghost 1 for vertex and not cell
        if load_ghost:  # nppb -> points per block side
            ppbs = ppbs_wghost
        else:
            ppbs = bsize + 1

        for name in ["points", "attr", "x", "y", "z"]:
            if name in load_names:
                data_out[name] = initialize_data_array(nblock_out, ppbs,
                                        int if name == "attr" else float,
                                        True if name == "points" else False,
                                        is2D,)

        # Reading Attributs
        for n in range(1, nblock + 1):
            if (n in blocks) and ("attr" in load_names):
                data_out["attr"][blocks.index(n)] = file.read_block(
                    "i4", load_ghost, ppbs_wghost, is2D
                )
            else:
                file.skip_record("i4")

        # x value
        for n in range(1, nblock + 1):
            if (n in blocks) and ("points" in load_names):
                data_out["points"][blocks.index(n), 0] = file.read_block(
                    "f8", load_ghost, ppbs_wghost, is2D)
            elif (n in blocks) and ("x" in load_names):
                data_out["x"][blocks.index(n)] = file.read_block(
                    "f8", load_ghost, ppbs_wghost, is2D)
            else:
                file.skip_record("f8")

        # y value
        for n in range(1, nblock + 1):
            if (n in blocks) and ("points" in load_names):
                data_out["points"][blocks.index(n), 1] = file.read_block(
                    "f8", load_ghost, ppbs_wghost, is2D)
            elif (n in blocks) and ("y" in load_names):
                data_out["y"][blocks.index(n)] = file.read_block(
                    "f8", load_ghost, ppbs_wghost, is2D)
            else:
                file.skip_record("f8")

        # z value
        for n in range(1, nblock + 1):
            if (n in blocks) and ("points" in load_names):
                data_out["points"][blocks.index(n), 2] = file.read_block(
                    "f8", load_ghost, ppbs_wghost, is2D)
            elif (n in blocks) and ("z" in load_names):
                data_out["z"][blocks.index(n)] = file.read_block(
                    "f8", load_ghost, ppbs_wghost, is2D)
            else:
                file.skip_record("f8")

    if as_matrix:
        data_out = array2matrix(data_out, ppbs, nblock_out, is2D)

    return data_out, bsize, nblock


def XXD_set_default_args(filename, load_names, read_default, is2D):
    """Convert input arguments to default values (instead of None)

    Parameters
    ----------
    filename : str
        filename for a .X3D or.X2D file
    load_names : set, optional
        Set of names to load.
        If None it it will default to set("points") if read_default=True,
        by default None
        options:
            "points": Load all points as *nblock x 3 x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
            "attr": Load vertex attributes as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
            "x": Load x-vertex coordinates as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - not loaded if points are in set
            "y": Load y-vertex coordinates as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - not loaded if points are in set
            "z": Load z-vertex coordinates as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - not loaded if points are in set
    read_default : bool, optional
        Flag for setting defaults, by default True
    is2D : bool, optional
        Flag for indicating if the data is 2D or not.
        By default None -> use file extension *.X2D* -> True

    Returns
    -------
    load_names: set
        See input load_names arguments
    is2D: bool
        See input is2D arguments
    """
    if load_names is None:
        load_names = set()

    if read_default:
        load_names.update({"points"})

    if is2D is None:
        if ".X2D" in filename:
            is2D = True
        else:
            is2D = False
    return load_names, is2D


### ---------------------------------- RST files -------------------------- ###
def RST2dict(filename, load_names=None, read_default=True, blocks=None,
             load_ghost=False, as_matrix=False, is2D=False, vectors=None):
    """Reading a EllipSys .RST file and return dict with data and bsize
    and nblock

    Parameters
    ----------
    filename : str
        filename for a .RST file (currently only support .RST.01)
    load_names : set, optional
        Set of names to load.
        If None it it will default to set("p", "Vel") if read_default=True,
        by default None
        options:
            "p": Load pressure *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
            "u": Load u-velocity component *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - only need if only one component is needed
            "v": Load v-velocity component *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - only need if only one component is needed
            "w": Load w-velocity component *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - only need if only one component is needed
            "Vel": Load Velocity as a vector *nblock x 3 x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
    read_default : bool, optional
        Flag for setting defaults, by default True
    blocks : list, optional
        List of block to load. By default None -> load all blocks
    load_ghost : bool, optional
        Flag for loading ghost vertex points. `load_ghost=True`-> bsize+3,
        by default False -> bsize+1
    as_matrix : bool, optional
        Flag for making the output as matrix
        *nblock x bsize+1 x bsize+1 (x bsize+1)* ,
        by default False
    is2D : bool, optional
        Flag for indicating if the data is 2D or not.
        By default None -> use file extension *.X2D* -> True
    vectors : dict, optional
        Relationship between field names and vectors.
        Example: vector={"Vel":["u", "v", "w"]} will load a vector with
        the name Vel and the first component is u.
        By default None -> {"Vel":["u", "v", "w"]}

    Returns
    -------
    data_out: dict
        output data, see load_names for information
    bsize: int
        Block size (number of nodes -> +1 for number of vertices)
    nblock: int
        Number of blocks in the file (not the number of blocks loaded)
    """
    # Setting default arguments
    load_names, vectors, fields_in_vectors = RST_set_default_args(
        load_names, read_default, vectors
    )

    bsize = None
    nblock = None
    data_out = dict()
    with FortranFile_wskip(filename, "r") as file:
        # Reading header
        read_restart_version(file)

        # Reading main file data
        read_more = True
        while read_more:
            # Reading data header (name, data-type, array_type, size)
            name, dtype, action, size = read_data_header(file)

            # Reading data
            if name == "bsize":
                bsize = read_data(file, dtype, action, size, nblock, True)
                # Inialize arrays
                ppbs_wghost = (bsize + 3)
                if load_ghost:  # nppb -> points per block side
                    ppbs = ppbs_wghost
                else:
                    ppbs = bsize + 1
            elif name == "nblock":
                nblock = read_data(file, dtype, action, size, nblock, True)
                if blocks is None:
                    blocks = range(1, nblock + 1)
                nblock_out = len(blocks)
            elif name == "stop":
                read_more = False
            elif (name in load_names) or (
                (name in fields_in_vectors)
                    and (fields_in_vectors[name] in load_names)):

                dtype_np = float if dtype == "REAL" else int

                if name in fields_in_vectors:  # Reading vector field
                    vec_name = fields_in_vectors[name]
                    if vec_name not in data_out:
                        data_out[vec_name] = initialize_data_array(
                            nblock_out, ppbs, dtype_np, True, is2D)
                    data_out[vec_name][
                        :, vectors[vec_name].index(name), :
                    ] = read_data(
                        file,
                        dtype,
                        action,
                        size,
                        nblock,
                        data_out[vec_name][
                            :, vectors[vec_name].index(name), :
                        ],
                        blocks,
                        load_ghost,
                        ppbs,
                        is2D,
                    )
                else:  # Reading scalar field
                    data_out[name] = read_data(file, dtype, action, size,
                                               nblock, None, blocks,
                                               load_ghost, ppbs, is2D)

            else:
                read_data(file, dtype, action, size, nblock, skip=True)

    if as_matrix:
        data_out = array2matrix(data_out, ppbs, nblock_out, is2D)

    return data_out, bsize, nblock


def RST_set_default_args(load_names, read_default, vectors):
    """Convert input arguments to default values (instead of None)

    Parameters
    ----------
    load_names : set, optional
        Set of names to load.
        If None it it will default to set("p", "Vel") if read_default=True,
        by default None
        options:
            "p": Load pressure *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
            "u": Load u-velocity component *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - only need if only one component is needed
            "v": Load v-velocity component *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - only need if only one component is needed
            "w": Load w-velocity component *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - only need if only one component is needed
            "Vel": Load Velocity as a vector *nblock x 3 x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
    read_default : bool, optional
        Flag for setting defaults, by default True
    vectors : dict, optional
        Relationship between field names and vectors.
        Example: vector={"Vel":["u", "v", "w"]} will load a vector with
        the name Vel and the first component is u.
        By default None -> {"Vel":["u", "v", "w"]}

    Returns
    -------
    load_names: set
        See input load_names arguments
    vectors: dict
        See input vectors arguments
    fields_in_vectors: dict
        Relationship between field names and vectors.
        Example: {"u":"Vel", "v":"Vel", "w":"Vel"}
    """
    # Updating default settings
    if load_names is None:
        load_names = set()
    if read_default:
        load_names.update(["p", "Vel"])
    if vectors is None:
        vectors = dict()

    # Default vector for velocity
    if not any("u" in fields for fields in vectors.values()):
        vectors["Vel"] = ["u", "v", "w"]

    # Dict containing field -> vector connection
    fields_in_vectors = dict()
    for name, fields in vectors.items():
        for field in fields:
            fields_in_vectors[field] = name
    return load_names, vectors, fields_in_vectors


def RST2names(filename, name_out="fields"):
    """Extract field names from a .RST file without loading
    the file into memory

    Parameters
    ----------
    filename : str
        filename for a .RST file
    name_out : str, optional
        Flag for which kind of data is read out, by default "fields"
        Options:
            'fields': only data fields (same shape as grid data)
            'fields info': only data fields as well as information about
                the data (dtype, action, size)
            'all': outputs all data names
            'all info': returns all data names and information about
                the data (dtype, action, size)

    Returns
    -------
    dict or set
        returns fields form the RST file (set if 'info' in name_out else dict)
    """
    if "info" in name_out:
        data_out = dict()
    else:
        data_out = set()

    with FortranFile_wskip(filename, "r") as file:
        # Reading header
        read_restart_version(file)

        # Creating ouput data lists
        read_more = True
        nblock = None
        while read_more:
            # Reading data header (name, data-type, array_type, size)
            name, dtype, action, size = read_data_header(file)

            # Reading data (only reading nblock)
            data = read_data(
                file, dtype, action, size, nblock, skip=not name == "nblock")

            # Setting bsize, nblock variables and testing for stop read
            if name == "stop":
                read_more = False
            elif name == "nblock":
                nblock = data

            if "fields" in name_out:
                if is_data_field(dtype, action):
                    if "info" in name_out:
                        data_out[name] = dict(
                            dtype=dtype, action=action, size=size)
                    else:
                        data_out.add(name)
            elif "all" in name_out:
                if "info" in name_out:
                    data_out[name] = dict(
                        dtype=dtype, action=action, size=size)
                else:
                    data_out.add(name)
        return data_out


### -------------------------- Simulations (X3D/X2D and RST files) -------- ###
def sim2names(filename_RST, name_out="fields"):
    """Extract field names from a .RST and .XXD files without loading
    the file into memor

    Parameters
    ----------
    filename : str
        filename for a .RST file
    name_out : str, optional
        Flag for which kind of data is read out, by default "fields"
        Options:
            'fields': only data fields (same shape as grid data)
            'fields info': only data fields as well as information about
                the data (dtype, action, size)
            'all': outputs all data names
            'all info': returns all data names and information about
                the data (dtype, action, size)

    Returns
    -------
    dict or set
        returns fields form the RST file (set if 'info' in name_out else dict)
    """
    data_out = RST2names(filename_RST, name_out)
    if isinstance(data_out, set):
        data_out.add("attr")
    else:
        data_out["attr"] = dict(
            dtype="INTEGER", action="SCATTER", size=data_out["u"]["size"]
        )
    return data_out


def sim2dict(filename_RST, filename_XXD=None, load_names=None,
             read_default=True, blocks=None, load_ghost=False,
             as_matrix=False, is2D=None, vectors=None,):
    """Reading a EllipSys .RST file and return dict with data and bsize
    and nblock

    Parameters
    ----------
    filename_RST : str
        filename for a .RST file (currently only support .RST.01)
    filename_XXD : str
        filename for a .X3D or.X2D file.
        By default None -> infere from filename_RST project-name.
        Ex. grid.RST.01 -> grid.X3D if in path otherwise grid.X2D
    load_names : set, optional
        Set of names to load.
        If None it it will default to set("p", "Vel") if read_default=True,
        by default None
        options:
            "points": Load all points as *nblock x 3 x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
            "attr": Load vertex attributes as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
            "x": Load x-vertex coordinates as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - not loaded if points are in set
            "y": Load y-vertex coordinates as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - not loaded if points are in set
            "z": Load z-vertex coordinates as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - not loaded if points are in set
            "p": Load pressure *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
            "u": Load u-velocity component *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - only need if only one component is needed
            "v": Load v-velocity component *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - only need if only one component is needed
            "w": Load w-velocity component *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - only need if only one component is needed
            "Vel": Load Velocity as a vector *nblock x 3 x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
    read_default : bool, optional
        Flag for setting defaults, by default True
    blocks : list, optional
        List of block to load. By default None -> load all blocks
    load_ghost : bool, optional
        Flag for loading ghost vertex points. `load_ghost=True`-> bsize+3,
        by default False -> bsize+1
    as_matrix : bool, optional
        Flag for making the output as matrix
        *nblock x bsize+1 x bsize+1 (x bsize+1)* ,
        by default False
    is2D : bool, optional
        Flag for indicating if the data is 2D or not.
        By default None -> use file extension *.X2D* -> True
    vectors : dict, optional
        Relationship between field names and vectors.
        Example: vector={"Vel":["u", "v", "w"]} will load a vector with
        the name Vel and the first component is u.
        By default None -> {"Vel":["u", "v", "w"]}

    Returns
    -------
    data_out: dict
        output data, see load_names for information
    bsize: int
        Block size (number of nodes -> +1 for number of vertices)
    nblock: int
        Number of blocks in the file (not the number of blocks loaded)
    """
    # If no grid filename is set default values
    filename_XXD, load_names, vectors, _, is2D = sim_set_default_args(
        filename_RST, filename_XXD, load_names, read_default, vectors, is2D)

    # Read grid data
    XXD, bsize_XXD, nblock_XXD = XXD2dict(
        filename_XXD, load_names, read_default,
        blocks, load_ghost, as_matrix, is2D)

    # Read restart data
    RST, bsize_RST, nblock_RST = RST2dict(
        filename_RST, load_names, read_default,
        blocks, load_ghost, as_matrix, is2D, vectors)

    # Reduce number of grid cells if the gridlevel is not 1:
    XXD, bsize_XXD = reduce_gridlevel(
        XXD, bsize_XXD, bsize_RST, nblock_XXD,
        is2D, load_ghost, blocks, as_matrix)

    # Testing bsize and nblock
    if bsize_RST != bsize_XXD:
        raise ValueError(
            f"bsize for the XXD-file ({bsize_XXD}) and RST-file ({bsize_RST}) do not match"
        )
    if nblock_XXD != nblock_RST:
        raise ValueError(
            f"nblock for the XXD-file ({nblock_XXD}) and RST-file ({nblock_RST}) do not match"
        )
    RST.update(XXD)
    return RST, bsize_XXD, nblock_XXD


def sim_set_default_args(filename_RST, filename_XXD, load_names, read_default,
                         vectors, is2D):
    """Convert input arguments to default values (instead of None)

    Parameters
    ----------
    filename_RST : str
        filename for a .RST file (currently only support .RST.01)
    filename_XXD : str
        filename for a .X3D or.X2D file.
        By default None -> infere from filename_RST project-name.
        Ex. grid.RST.01 -> grid.X3D if in path otherwise grid.X2D
    load_names : set, optional
        Set of names to load.
        If None it it will default to set("points") if read_default=True,
        by default None
        options:
            "points": Load all points as *nblock x 3 x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
            "attr": Load vertex attributes as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
            "x": Load x-vertex coordinates as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - not loaded if points are in set
            "y": Load y-vertex coordinates as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - not loaded if points are in set
            "z": Load z-vertex coordinates as *nblock x (bsize+1)**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - not loaded if points are in set
            "p": Load pressure *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
            "u": Load u-velocity component *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - only need if only one component is needed
            "v": Load v-velocity component *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - only need if only one component is needed
            "w": Load w-velocity component *nblock x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
                - only need if only one component is needed
            "Vel": Load Velocity as a vector *nblock x 3 x bsize**(3 or 2)*
                (reshaped as matrix if as_matrix=True)
    read_default : bool, optional
        Flag for setting defaults, by default True
    vectors : dict, optional
        Relationship between field names and vectors.
        Example: vector={"Vel":["u", "v", "w"]} will load a vector with
        the name Vel and the first component is u.
        By default None -> {"Vel":["u", "v", "w"]}
    is2D : bool, optional
        Flag for indicating if the data is 2D or not.
        By default None -> use file extension *.X2D* -> True

    Returns
    -------
    filename_XXD : str
        See input filename_XXD
    load_names: set
        See input load_names arguments
    vectors: dict
        See input vectors arguments
    fields_in_vectors: dict
        Relationship between field names and vectors.
        Example: {"u":"Vel", "v":"Vel", "w":"Vel"}
    is2D: bool
        See input is2D arguments
    """
    # If no grid filename is set
    if filename_XXD is None:
        path = os.path.dirname(filename_RST)
        if path == "":
            path = "."
        proj_name = os.path.basename(filename_RST)
        proj_name = proj_name[: proj_name.index(".RST")]
        if proj_name + ".X3D" in os.listdir(path):
            filename_XXD = os.path.join(path, proj_name + ".X3D")
        elif proj_name + ".X2D" in os.listdir(path):
            filename_XXD = os.path.join(path, proj_name + ".X2D")
        else:
            raise ValueError(
                "Could not find grid a suitable .XXD file in current dir with project name: %s"
                % proj_name
            )

    load_names, is2D = XXD_set_default_args(
        filename_XXD, load_names, read_default, is2D
    )
    load_names, vectors, fields_in_vectors = RST_set_default_args(
        load_names, read_default, vectors
    )
    return filename_XXD, load_names, vectors, fields_in_vectors, is2D


### -------------------------------- adinput files ------------------------ ###
def adinput2dict(filename, ADs=None, close_disc=True, nstep=-1,
                 as_matrix=False):
    """Reads adinput.dat file and the associated output files if present

    Parameters
    ----------
    filename : str
        Filename (including path) of the adinput.dat file
    ADs : list, optional
        List of ADs to read. By default None -> read all ADs
    close_disc : bool, optional
        Flag for closing the AD disc. By default True
    nstep : int, optional
        Index of the output data to read. By default -1 -> the last element
    as_matrix : bool, optional
        Flag for making the output as matrix *nr x ntheta x 3* ,
        by default False

    Returns
    -------
    dict
        Items:
        AD%d:
            name: AD disc name
            discgrid_name: filename for AD disc grid
            R: Rotor radius for AD
            loc: location of the AD disc list(x, y, z)
            o1: Orientation vector 1
            o2: Orientation vector 2
            o3: Orientation vector 3
            nr: Number of radial points
            ntheta: Number of azimuthal points
            discgrid: dict containing AD disc grid data
                points: Grid points *nr*ntheta x 3*
                        (reshaped as matrix if as_matrix=True)
                FN: Normal force *nr*ntheta*
                    (reshaped as matrix if as_matrix=True)
                FT: Tangential force *nr*ntheta*
                    (reshaped as matrix if as_matrix=True)
                VN: Normal velocity *nr*ntheta*
                    (reshaped as matrix if as_matrix=True)
                VT: Tangential velocity *nr*ntheta*
                    (reshaped as matrix if as_matrix=True)
    """
    path = os.path.dirname(filename)
    if not path:
        path = "."
    # reading adinput as dict
    out = dict()
    with open(filename, "r") as file:
        # Skipping the header
        file.readline()

        for i_AD, line in enumerate(file, 1):
            # Splitting line
            line = line.strip().split()
            if not line:  # Stopping if line is empty
                break
            if (not ADs is None) and (not i_AD in ADs):
                continue

            # Creating AD container
            AD_name = "AD%d" % i_AD
            out[AD_name] = dict()

            # AD name and discgrid name
            out[AD_name]["name"] = line[0]
            out[AD_name]["discgrid_name"] = line[1]

            # Reading rotor radius
            out[AD_name]["R"] = float(line[2]) / 2

            # Reading turbine location
            out[AD_name]["loc"] = np.array(line[3:6], dtype=float)

            # Reading turbine oriantation (ensuring unit vector)
            o1 = np.array(line[6:9], dtype=float)
            out[AD_name]["o1"] = o1 / np.linalg.norm(o1)
            o2 = np.array(line[9:12], dtype=float)
            out[AD_name]["o2"] = o2 / np.linalg.norm(o2)
            o3 = np.cross(o1, o2)
            out[AD_name]["o3"] = o3 / np.linalg.norm(o3)

            # Reading the disc-grid or making a dummy
            out[AD_name]["discgrid"] = dict()
            out[AD_name]["discgrid"]["points"], nr, ntheta = read_discgrid(
                out[AD_name]["discgrid_name"], path, close_disc, as_matrix
            )
            out[AD_name]["nr"] = nr
            out[AD_name]["ntheta"] = ntheta

            # Reading disc data (if present)
            files_in_path = np.array(os.listdir(path))
            for field_name in ["FN", "FT", "VN", "VT"]:
                loc = np.array(
                    [
                        (".AS%s%03d" % (field_name, i_AD) in name)
                        for name in files_in_path
                    ]
                )
                if any(loc):
                    out[AD_name]["discgrid"][field_name] = read_discgrid_field(
                        os.path.join(path, files_in_path[loc][0]),
                        nr,
                        ntheta,
                        close_disc,
                        nstep,
                        as_matrix,
                    )

    return out


def read_discgrid(filename, path, close_disc, as_matrix):
    """Reading the discgrid file

    Parameters
    ----------
    filename : str
        Filename for the disc grid
    path : str
        Path for the adinput.dat file
    close_disc : bool, optional
        Flag for closing the AD disc.
    as_matrix : bool, optional
        Flag for making the output as matrix *nr x ntheta x 3* ,
        by default False

    Returns
    -------
    points: ndarray
        Array with grid points *nr*ntheta x 3*
        (reshaped as matrix if as_matrix=True)
    nr: int
        Number of points in the radial direction
    ntheta: int
        Number of points in the azimuthal direction
    """
    if os.path.isfile(os.path.join(path, filename)):
        # Reading the grid file
        with open(os.path.join(path, filename), "r") as gridfile:
            nr, ntheta, ndim = np.array(
                gridfile.readline().strip().split(), dtype=int
            )
            if close_disc:
                ntheta += 1
                points = np.zeros((nr, ntheta, ndim))
                points[:, :-1, :] = np.loadtxt(gridfile).reshape(
                    [nr, ntheta - 1, ndim]
                )
                points[:, -1, :] = points[:, 0, :]
            else:
                points = np.loadtxt(gridfile).reshape([nr, ntheta, ndim])
    else:
        # Making a dummy grid instead of reading
        nr, ntheta, ndim = [2, 100, 3]
        if close_disc:
            theta = np.linspace(-np.pi, np.pi, ntheta)
        else:
            theta = np.linspace(-np.pi, np.pi, ntheta + 1)[:-1]
        points = np.zeros((nr, ntheta, ndim))
        points[1, :, 0] = np.cos(theta)
        points[1, :, 1] = np.sin(theta)
    if as_matrix:
        return points.T, nr, ntheta
    else:
        return points.T.reshape((ndim, nr * ntheta)), nr, ntheta


def read_discgrid_field(filename, nr, ntheta, close_disc, nstep, as_matrix):
    """Reads a discgrid field data file

    Parameters
    ----------
    filename : str
        Filename for the disc grid field data
    nr: int
        Number of points in the radial direction
    ntheta: int
        Number of points in the azimuthal direction
    close_disc : bool, optional
        Flag for closing the AD disc.
    nstep : int, optional
        Index of the output data to read. By default -1 -> the last element
    as_matrix : bool, optional
        Flag for making the output as matrix *nr x ntheta x 3* ,
        by default False

    Returns
    -------
    ndarray
        Field data *nr*ntheta x 3* (reshaped as matrix if as_matrix=True)
    """
    # Cell center data
    data = np.loadtxt(filename)
    if len(data.shape) == 1:
        data = data[1:]
    else:
        data = data[nstep, 1:]
    if close_disc:
        points = data.reshape([ntheta - 1, nr - 1])
    else:
        points = data.reshape([ntheta, nr - 1])[:-1, :]

    if as_matrix:
        return points
    else:
        return points.flatten()


### -------------------------- Miscellaneous files ------------------------ ###
def initialize_data_array(nblocks, bsize, dtype=float, vec=False, is2D=False,
                          as_matrix=False):
    """Initlize a data array

    Parameters
    ----------
    nblocks : int
        Number of blocks in the array
    bsize : int
        The block size (with or without ghost cells)
    dtype : float, int, optional
        The array data type, by default float
    vec : bool, optional
        Flag for indicating if data is a vector or not, by default False
    is2D : bool, optional
        Flag for indicating if the data is 2D or not. By default False
    as_matrix : bool, optional
        Flag for indicating if data should be in matrix form. By default False
    Returns
    -------
    ndarray
        zeros with the shape *nblocks x bsize**(3 or 2)* of dtype
    """

    if is2D:
        if vec:
            if as_matrix:
                return np.zeros([nblocks, 3, bsize, bsize], dtype=dtype)
            else:
                return np.zeros([nblocks, 3, bsize**2], dtype=dtype)
        else:
            if as_matrix:
                return np.zeros([nblocks, bsize, bsize], dtype=dtype)
            else:
                return np.zeros([nblocks, bsize**2], dtype=dtype)
    else:
        if vec:
            if as_matrix:
                return np.zeros([nblocks, 3, bsize, bsize, bsize], dtype=dtype)
            else:
                return np.zeros([nblocks, 3, bsize**3], dtype=dtype)
        else:
            if as_matrix:
                return np.zeros([nblocks, bsize, bsize, bsize], dtype=dtype)
            else:
                return np.zeros([nblocks, bsize**3], dtype=dtype)


def read_restart_version(file):
    """Read the restart version from the RST file (file header)

    Parameters
    ----------
    file : FortranFile or FortranFile_wskip
        File handle

    Returns
    -------
    restart_version : str
        Restart version number
    restart_version_type : str
        Restart version type
    """
    restart_version = file.read_record("S20")[0].decode().strip()
    restart_version_type = file.read_record("S20")[0].decode().strip()
    return restart_version, restart_version_type


def read_data_header(file):
    """Reads the RST data header

    Parameters
    ----------
    file : FortranFile or FortranFile_wskip
        File handle

    Returns
    -------
    name : str
        Data name
    dtype : str
        Data type
    action : str
        Action name
    size : int
        Size of the data header
    """
    name = file.read_record("S20")[0].decode().strip()
    dtype = file.read_record("S20")[0].decode().strip()
    action = file.read_record("S20")[0].decode().strip()
    size = file.read_record("I")[0]
    return name, dtype, action, size


def read_data(file, dtype, action, size, nblock, data=None, blocks=None,
              load_ghost=False, bsize=0, is2D=False, skip=False):
    """Reads a data block using the information from the data header

    Parameters
    ----------
    file : FortranFile or FortranFile_wskip
        File handle
    dtype : str
        Data type
    action : str
        Action name
    size : int
        Size of the data header
    nblock : int
        Number of blocks for the data type
    data : None or ndarray, optional
        Data object to store data. Used to add data to already existing array
        instead of allocating new arrays.
        By default None -> Creating new data object
    blocks : list, optional
        List of block to loaded. Need to be given as an array.
        By default None -> Error
    load_ghost : bool, optional
        Flag for loading ghost vertex points.
        `load_ghost=True`-> bsize+3, by default False -> bsize+1
    bsize : int, optional
        Block size without ghost cells
    is2D : bool, optional
        Flag for indicating if the data is 2D or not.
    skip : bool, optional
        Flag for skipping a data entry, by default False

    Returns
    -------
    ndarray
        data array of dtype
    """

    bsize_wghost = bsize if load_ghost else bsize + 2
    # Integer of size 1
    if (
        ((dtype == "INTEGER") or (dtype == "LOGICAL"))
        and (size == 1)
        and (action == "BCAST")
    ):
        if skip:
            file.skip_record("I")
        else:
            data = file.read_record("I")[0]

    # Integers with size large than 1 and BCST (common to all processes)
    elif ((dtype == "INTEGER") or (dtype == "LOGICAL")) and (
        action == "BCAST"
    ):
        if skip:
            file.skip_record("i4")
        else:
            data = file.read_record("i4")

    # Integers with size larger than 1 and SCATTER (from each block)
    elif ((dtype == "INTEGER") or (dtype == "LOGICAL")) and (
        action == "SCATTER"
    ):
        if skip:
            for iblock in range(1, nblock + 1):
                file.skip_record("i4")
        else:
            if data is None:
                data = initialize_data_array(
                    len(blocks), bsize, int, False, is2D
                )
            for iblock in range(1, nblock + 1):
                if iblock in blocks:
                    data[blocks.index(iblock), :] = file.read_block(
                        "i4", load_ghost, bsize_wghost, is2D
                    )
                else:
                    file.skip_record("i4")

    # Float of size 1
    elif (dtype == "REAL") and (size == 1) and (action == "BCAST"):
        if skip:
            file.skip_record("f")
        else:
            data = file.read_record("f")[0]

    # Float with size large than 1 and BCST (common to all processes)
    elif (dtype == "REAL") and (action == "BCAST"):
        if skip:
            file.skip_record("f8")
        else:
            data = file.read_record("f8")

    # Float with size larger than 1 and SCATTER (from each block)
    elif (dtype == "REAL") and (action == "SCATTER"):
        if skip:
            for iblock in range(1, nblock + 1):
                file.skip_record("f8")
        else:
            if data is None:
                data = initialize_data_array(
                    len(blocks), bsize, float, False, is2D
                )
            for iblock in range(1, nblock + 1):
                if iblock in blocks:
                    if is2D:
                        ccdata = file.read_record("f8").reshape(
                            (bsize_wghost, bsize_wghost)
                        )
                        # interpolating cell-centered values to vertices
                        cvdata = shift2vertex2d(ccdata, load_ghost)
                        data[blocks.index(iblock), :] = cvdata.reshape(
                            1, bsize**2
                        )
                    else:
                        ccdata = file.read_record("f8").reshape(
                            (bsize_wghost, bsize_wghost, bsize_wghost)
                        )
                        # interpolating cell-centered values to vertices
                        cvdata = shift2vertex3d(ccdata, load_ghost)
                        data[blocks.index(iblock), :] = cvdata.reshape(
                            1, bsize**3
                        )
                else:
                    file.skip_record("f8")
    # Char with set size
    elif (dtype == "CHARACTER") and (action == "BCAST"):
        if skip:
            file.skip_record("S%i" % size)
        else:
            data = file.read_record("S%i" % size)[0].decode().strip()
    return data


def is_data_field(dtype, action):
    """Test if field is a data field

    Parameters
    ----------
    dtype : str
        Data type
    action : str
        Action name

    Returns
    -------
    bool
        Flag for indicating if the data type is a data field
    """
    return ((dtype == "REAL") or (dtype == "INTERGER")) and action == "SCATTER"


def shift2vertex2d(cc, load_ghost=False):
    """Computes the average of every set of four adjacent cell-centers,
     i.e. interpolates cell-centered variables to the vertices in a 2D grid

    Parameters
    ----------
    cc: ndarray
        shape (ni, ni) containing variables at cell-centers

    Returns
    -------
    cv: ndarray
       shape (ni-2, ni-2) containing the interpolated values for the vertices
    """

    if load_ghost:
        no = np.shape(cc)[0]
        cc_dummy = np.zeros((no+2, no+2))
        cc_dummy[1:-1, 1:-1] = cc
        cc = cc_dummy

    ni = np.shape(cc)[0]
    cv = np.zeros((ni - 2, ni - 2))
    for i in range(ni - 2):
        for j in range(ni - 2):
            cv[i, j] = 0.25 * (
                cc[i, j] + cc[i + 1, j] + cc[i, j + 1] + cc[i + 1, j + 1]
            )
    return cv


def shift2vertex3d(cc, load_ghost=False):
    """Computes the average of every set of eight adjacent cell-centers,
     i.e. interpolates cell-centered variables to the vertices in a 3D grid

    Parameters
    ----------
    cc: ndarray
        shape (ni, ni, ni) containing variables at cell-centers

    Returns
    -------
    cv: ndarray
        shape (ni-2, ni-2, ni-2) containing the interpolated values for
        the vertices
    """

    if load_ghost:
        no = np.shape(cc)[0]
        cc_dummy = np.zeros((no+2, no+2, no+2))
        cc_dummy[1:-1, 1:-1, 1:-1] = cc
        cc = cc_dummy

    ni = np.shape(cc)[0]
    cv = np.zeros((ni - 2, ni - 2, ni - 2))
    for i in range(ni - 2):
        for j in range(ni - 2):
            for k in range(ni - 2):
                cv[i, j, k] = 0.125 * (
                    cc[i, j, k]
                    + cc[i + 1, j, k]
                    + cc[i, j + 1, k]
                    + cc[i + 1, j + 1, k]
                    + cc[i, j, k + 1]
                    + cc[i + 1, j, k + 1]
                    + cc[i, j + 1, k + 1]
                    + cc[i + 1, j + 1, k + 1]
                )
    return cv


def reduce_gridlevel(XXD, bsize_XXD, bsize_RST, nblock_XXD, is2D, load_ghost,
                     blocks, as_matrix):
    """
    Reduces the grid level of a XDD grid to match the grid level of the
    corresponding RST data.

    Parameters
    ----------
    XXD : dict
        dict containing the grid data.
    bsize_XXD : int
        block size of grid data.
    bsize_RST : int
        block size of RST data.
    nblock_XXD : int
        number of blocks.
    is2D : bool
        flag for dimension.
    load_ghost : bool
        flag if ghost cells should be loaded.
    blocks : list
        list of blocks.
    as_matrix : bool
        flag to indicate data array shape.

    Returns
    -------
    XXD : dict
        dict containing the grid data.
    bsize_XXD : int
        new block size of grid data.

    """

    # Determine grid level
    glevel_RST = int(np.log2(bsize_XXD / bsize_RST) + 1)

    # Iterate over grid levels if other than 1
    if glevel_RST != 1:

        # Reshape data to matrix if necessary
        if not as_matrix:
            if load_ghost:
                XXD = array2matrix(XXD, bsize_XXD + 3, nblock_XXD, is2D)
            else:
                XXD = array2matrix(XXD, bsize_XXD + 1, nblock_XXD, is2D)

        # Setting up blocks to iterate over
        if blocks is None:
            blocks = range(1, nblock_XXD + 1)

        # Iterate over every gridlevel and remove points
        for i in range(glevel_RST - 1):

            # Get current block size
            bsize_XXD_old = np.shape(XXD[list(XXD.keys())[0]])[-1]
            bsize_XXD_new = (int((bsize_XXD_old - 1) / 2 + 2) if load_ghost
                             else int((bsize_XXD_old - 1) / 2 + 1))

            # Initialize new dictionary and allocate memory
            XXD_new = {}
            for key in XXD.keys():
                XXD_new[key] = initialize_data_array(
                    len(blocks),
                    bsize_XXD_new,
                    int if key == "attr" else float,
                    True if key == "points" else False,
                    is2D,
                    True,
                )

            # Remove cells
            XXD = remove_cells(XXD, XXD_new, is2D, load_ghost)

            bsize_XXD = int(bsize_XXD / 2)

        # Undo reshaping if necessary
        if not as_matrix:
            if load_ghost:
                XXD = matrix2array(XXD, bsize_XXD + 3, nblock_XXD, is2D)
            else:
                XXD = matrix2array(XXD, bsize_XXD + 1, nblock_XXD, is2D)

    return XXD, bsize_XXD


def array2matrix(data_out, blocksize, nblock, is2D):
    """
    Reshapes data to a matrix-like structure similar to the block-structure
    of the grid.

    Parameters
    ----------
    data_out : dict
        dict of data to reshape.
    blocksize : int
        number of points in each dimension for each block.
    nblock : int
        number of blocks.
    is2D : bool
        flag if simulation is 2D.

    Returns
    -------
    data_out : dict
        reshaped dictionary.

    """
    for key, data in data_out.items():
        mat_size = (blocksize, blocksize) if is2D else (
            blocksize, blocksize, blocksize)

        if len(data.shape) == 3:
            data_out[key] = data.reshape((nblock, 3) + mat_size)
        else:
            data_out[key] = data.reshape((nblock,) + mat_size)
    return data_out


def matrix2array(data_out, blocksize, nblock, is2D):
    """
    Reshapes data to an array-like structure from a matrix structure

    Parameters
    ----------
    data_out : dict
        Dict of data to reshape.
    blocksize : int
        The number of points in each dimension for each block
        including ghost cells.
    nblock : int
        The number of blocks.
    is2D : bool
        Flag if simulation is 2D.

    Returns
    -------
    data_out : dict
        Reshaped dictionary.

    """
    for key, data in data_out.items():
        if is2D:
            if len(data.shape) == 3:  # Scalar
                data_out[key] = data.reshape((nblock, blocksize**2))
            elif len(data.shape) == 4:  # Vector
                data_out[key] = data.reshape(
                    (nblock, data.shape[1], blocksize**2))
        else:
            if len(data.shape) == 4:  # Scalar
                data_out[key] = data.reshape((nblock, blocksize**3))
            elif len(data.shape) == 5:  # Vector
                data_out[key] = data.reshape(
                    (nblock, data.shape[1], blocksize**3))
    return data_out


def remove_cells(XXD, XXD_new, is2D, load_ghost):
    """
    Removes every other cell from a XDD object, so reduces the grid level
    by one.

    Important: Arrays must be in matrix-form.

    Parameters
    ----------
    XXD : dict
        original dict containing grid information. Data must be in matrix
        form!
    XXD_new : dict
        new dict with empty arrays in correct dimensions and sizes.
    is2D : bool
        flag if 2D.
    load_ghost : bool
        flag if ghost cells should be loaded. Important for slicing.

    Returns
    -------
    XXD_new : dict
        new dict with full arrays and reduced size.

    """

    # Iterate over all items
    # The data shape can vary depending if vectors or scalars are iterated.
    for key, data in XXD.items():

        # Get old block size
        bsize_XXD_old = data.shape[-1]

        # Set ids of cells to remove
        if load_ghost:
            id_r = np.arange(2, bsize_XXD_old - 1, 2, dtype=int)
        else:
            id_r = np.arange(1, bsize_XXD_old, 2, dtype=int)

        # Set up slicer
        slicer = ()
        for i in range(len(data.shape)):
            slicer = slicer + np.index_exp[:]

        # Define mask
        mask = np.full(data.shape, False)
        for i in range(2 if is2D else 3):
            slicer_temp = list(slicer)
            slicer_temp[-(i+1)] = id_r
            slicer_temp = tuple(slicer_temp)
            mask[slicer_temp] = True
        mask = ~mask

        # Asign values
        XXD_new[key][slicer] = np.reshape(data[mask], XXD_new[key].shape)

    return XXD_new
