from vtk.numpy_interface import dataset_adapter as dsa
from vtk import (vtkMultiBlockDataSet, vtkPoints, vtkStructuredGrid,
                 vtkTransformFilter, vtkTransform, vtkArrayCalculator, vtkCompositeDataSet, vtkMatrix4x4)
import numpy as np
from .ellipsys2dict import XXD2dict, XXD_set_default_args, sim2dict, sim_set_default_args, adinput2dict

def XXD2vtk(filename, load_attr=True, blocks=None, load_ghost=False, is2D=None, mblock=None):
    """Reading EllipSys .X3D or .X2D files and returns a vtkMultiBlockDataSet.

    Parameters
    ----------
    filename : str  
        filename for a .X3D or.X2D file
    load_attr : bool, optional
        Flag for loading vertex attributes, by default True
    blocks : list, optional
        List of block to load. By default None -> load all blocks
    load_ghost : bool, optional
        Flag for loading ghost vertex points. `load_ghost=True`-> bsize+3, by default False -> bsize+1 
    is2D : bool, optional
        Flag for indicating if the data is 2D or not. By default None -> use file extension *.X2D* -> True
    mblock : vtkMultiBlockDataSet, optional
        VTK Multi Block Data Set which will be used to write data into. This is used for the ParaviewPlugin. By default None -> create a new vtkMultiBlockDataSet.

    Returns
    -------
    vtkMultiBlockDataSet
        VTK Multi Block Data Set containing the vertex data and attributes if `load_attr=True`.
    """    
    # Initializing multiblock
    if mblock is None:
        mblock = vtkMultiBlockDataSet()

    load_names, is2D = XXD_set_default_args(filename, {"attr"} if load_attr else None, True, is2D)

    XXD, bsize, nblock_out = XXD2dict(filename, load_names, True, blocks, load_ghost, False, is2D)

    # Computing domain size and number of blocks
    ppbs = bsize + 3 if load_ghost else bsize + 1
    if blocks is None:
        blocks = range(1, nblock_out + 1)
    nblock = len(blocks)

    # Setting the number of blocks
    mblock.SetNumberOfBlocks(nblock)

    # Setting block data
    for ib, i_block in enumerate(blocks):
        # Converting numpy array to vtk points
        points = vtkPoints()
        points.SetData(dsa.numpyTovtkDataArray(XXD["points"][ib].T, "Points"))

        # Creating the Structured Grid for the block
        sgrid = vtkStructuredGrid()
        if is2D:
            sgrid.SetExtent(0, ppbs - 1, 0, ppbs - 1, 0, 0)
        else:
            sgrid.SetExtent(0, ppbs - 1, 0, ppbs - 1, 0, ppbs - 1)
        sgrid.SetPoints(points)

        # Loading data
        if "attr" in XXD:
            sgrid.GetPointData().AddArray(dsa.numpyTovtkDataArray(XXD["attr"][ib].T, "attr"))

        # Adding the sgrid to mblock
        mblock.SetBlock(ib, sgrid)

        # Setting block name
        mblock.GetMetaData(ib).Set(vtkCompositeDataSet.NAME(), "Block %s" % i_block)

    return mblock

def sim2vtk(filename_RST, filename_XXD=None, load_names=None, read_default=True, blocks=None, load_ghost=False,
            is2D=None, vectors=None, mblock=None):
    """Reading EllipSys .RST.01 and .X3D (or .X2D) files and returns a vtkMultiBlockDataSet.

    Parameters
    ----------
    filename_RST : str  
        filename for a .RST file (currently only support .RST.01)
    filename_XXD : str  
        filename for a .X3D or.X2D file. By default None -> infere from filename_RST project-name. Ex. grid.RST.01 -> grid.X3D if in path otherwise grid.X2D
    load_names : set, optional
        Set of names to load. 
        If None it it will default to set("p", "Vel") if read_default=True, by default None
        options: (All options for a given RST file can be viewed with the RST2names)
            "attr": Load vertex attributes as *nblock x (bsize+1)**(3 or 2)* (reshaped as matrix if as_matrix=True)
            "p": Load pressure *nblock x bsize**(3 or 2)* (reshaped as matrix if as_matrix=True)
            "u": Load u-velocity component *nblock x bsize**(3 or 2)* (reshaped as matrix if as_matrix=True) - only need if only one component is needed
            "v": Load v-velocity component *nblock x bsize**(3 or 2)* (reshaped as matrix if as_matrix=True) - only need if only one component is needed
            "w": Load w-velocity component *nblock x bsize**(3 or 2)* (reshaped as matrix if as_matrix=True) - only need if only one component is needed
            "Vel": Load Velocity as a vector *nblock x 3 x bsize**(3 or 2)* (reshaped as matrix if as_matrix=True)
    read_default : bool, optional
        Flag for setting defaults, by default True
    blocks : list, optional
        List of block to load. By default None -> load all blocks
    load_ghost : bool, optional
        Flag for loading ghost vertex points. `load_ghost=True`-> bsize+3, by default False -> bsize+1 
    is2D : bool, optional
        Flag for indicating if the data is 2D or not. By default None -> use file extension *.X2D* -> True
    vectors : dict, optional
        Relationship between field names and vectors. Example: vector={"Vel":["u", "v", "w"]} will load a vector with the name Vel and the first component is u. By default None -> {"Vel":["u", "v", "w"]}
    mblock : vtkMultiBlockDataSet, optional
        VTK Multi Block Data Set which will be used to write data into. This is used for the ParaviewPlugin. By default None -> create a new vtkMultiBlockDataSet.

    Returns
    -------
    vtkMultiBlockDataSet
        VTK Multi Block Data Set containing the vertex data along with the cell center data from the RST file.
    """    
    # Initializing multiblock
    if mblock is None:
        mblock = vtkMultiBlockDataSet()

    # Reading data
    filename_XXD, load_names, vectors, fields_in_vectors, is2D = sim_set_default_args(filename_RST, filename_XXD,
                                                                                      load_names, read_default, vectors,
                                                                                      is2D)
    if "points" not in load_names:  # Forcing points to always be present
        load_names.add("points")
    sim, bsize, nblock_out = sim2dict(filename_RST, filename_XXD, load_names, read_default, blocks, load_ghost,
                                      False, is2D, vectors)

    # Computing domain size and number of blocks
    ppbs = bsize+3 if load_ghost else bsize+1
    if blocks is None:
        blocks = range(1, nblock_out+1)
    nblock = len(blocks)

    # Setting the number of blocks
    mblock.SetNumberOfBlocks(nblock)


    # Setting block data
    for ib, i_block in enumerate(blocks):
        # Converting numpy array to vtk points
        points = vtkPoints()
        points.SetData(dsa.numpyTovtkDataArray(sim["points"][ib].T, "Points"))

        # Creating the Structured Grid for the block
        sgrid = vtkStructuredGrid()
        if is2D:
            sgrid.SetExtent(0, ppbs-1, 0, ppbs-1, 0, 0)
        else:
            sgrid.SetExtent(0, ppbs-1, 0, ppbs-1, 0, ppbs-1)
        sgrid.SetPoints(points)

        # Loading data
        for name in sim.keys():
            if "points" == name:
                continue
            sgrid.GetPointData().AddArray(dsa.numpyTovtkDataArray(sim[name][ib].T, name))

        # Adding the sgrid to mblock
        mblock.SetBlock(ib, sgrid)

        # Setting block name
        mblock.GetMetaData(ib).Set(vtkCompositeDataSet.NAME(), "Block %s"%i_block)

    return mblock


def adinput2vtk(filename, ADs=None, close_disc=True, mblock=None):
    """Reads adinput.dat file and the associated output files if present

    Parameters
    ----------
    filename : str
        Filename (including path) of the adinput.dat file
    ADs : list, optional
        List of ADs to read. By default None -> read all ADs
    close_disc : bool, optional
        Flag for closing the AD disc. By default True
    mblock : vtkMultiBlockDataSet, optional
        VTK Multi Block Data Set which will be used to write data into. This is used for the ParaviewPlugin. By default None -> create a new vtkMultiBlockDataSet.

    Returns
    -------
    vtkMultiBlockDataSet
        VTK Multi Block Data Set containing each of the ADs as well as the field data.
    """    
    if mblock is None:
        mblock = vtkMultiBlockDataSet()

    # Reading data
    data = adinput2dict(filename, ADs, close_disc)

    mblock.SetNumberOfBlocks(len(data))

    for i_AD, (AD_name, AD_data) in enumerate(data.items()):

        # Making vtk points
        points = vtkPoints()
        points.SetData(dsa.numpyTovtkDataArray(AD_data["discgrid"]["points"].T, "Points"))

        # Making vtkStructuredGrid
        disc = vtkStructuredGrid()
        disc.SetExtent(0, AD_data["nr"] - 1, 0, AD_data["ntheta"] - 1, 0, 0)
        disc.SetPoints(points)

        # Setting data
        cell_data = disc.GetCellData()
        for name, data in AD_data["discgrid"].items():
            if name == "points":
                continue
            else:
                vtk_arr = dsa.numpy_support.numpy_to_vtk(data)
                vtk_arr.SetName(name)
                cell_data.AddArray(vtk_arr)

        # Transformation
        trans = vtkTransform()
        # Rotation matrix
        transfo_mat = vtkMatrix4x4()
        rot_mat = np.array([AD_data["o2"], AD_data["o3"], AD_data["o1"]]).T
        for i in range(0, 3):
            for j in range(0, 3):
                transfo_mat.SetElement(i, j, rot_mat[i, j])

        transfo_mat.SetElement(0, 3, AD_data["loc"][0])
        transfo_mat.SetElement(1, 3, AD_data["loc"][1])
        transfo_mat.SetElement(2, 3, AD_data["loc"][2])

        trans.SetMatrix(transfo_mat)
        # Translating
        #trans.Translate(*AD_data["loc"])
        # Scaling up
        trans.Scale(*(3 * [AD_data["R"]]))

        # Making transformation filter for the disc
        trans_poly = vtkTransformFilter()
        trans_poly.SetInputDataObject(0, disc)
        trans_poly.SetTransform(trans)
        trans_poly.Update()

        # Adding to multiblock output
        mblock.SetBlock(i_AD, trans_poly.GetOutputDataObject(0))
        mblock.GetMetaData(i_AD).Set(vtkCompositeDataSet.NAME(), AD_name)
    return mblock