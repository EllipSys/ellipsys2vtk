.. EllipSys2VTK documentation master file, created by
   sphinx-quickstart on Fri Apr 21 16:58:47 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to EllipSys2VTK's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   ../ellipsys2vtk

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
