ellipsys2vtk package
====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   ellipsys2vtk.tests

Submodules
----------

ellipsys2vtk.ellipsys2dict module
---------------------------------

.. automodule:: ellipsys2vtk.ellipsys2dict
   :members:
   :undoc-members:
   :show-inheritance:

ellipsys2vtk.ellipsys2pyvista module
------------------------------------

.. automodule:: ellipsys2vtk.ellipsys2pyvista
   :members:
   :undoc-members:
   :show-inheritance:

ellipsys2vtk.ellipsys2vtk module
--------------------------------

.. automodule:: ellipsys2vtk.ellipsys2vtk
   :members:
   :undoc-members:
   :show-inheritance:

ellipsys2vtk.util module
------------------------

.. automodule:: ellipsys2vtk.util
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: ellipsys2vtk
   :members:
   :undoc-members:
   :show-inheritance:
